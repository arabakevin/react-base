import React, { Component } from 'react';

class LogoutButton extends Component {
  render() {
    return (
      <button onClick={this.props.onClick}>
        Déconnexion
      </button>
    );
  }
}

export default LogoutButton; // Don’t forget to use export default!
