import React from 'react';

class ListItem extends React.Component  {

  render() {
  // Pas la peine de spécifier la clé ici :
    return <li>{this.props.value}</li>;
  }
}

export default ListItem; // Don’t forget to use export default!
