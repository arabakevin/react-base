import React from 'react';
import FormattedDate from './FormattedDate';

class ClockWithUpdate extends React.Component {
  constructor(props) {
    super(props);
    this.state = {date: new Date()};
  }

// La méthode componentDidMount() est exécutée après que la sortie du composant a été injectée dans le DOM
  componentDidMount() {
    this.timerID = setInterval(
      () => this.tick(),
      1000
    );
  }

  // Nous allons détruire le minuteur dans la méthode de cycle de vie componentWillUnmount()
  componentWillUnmount() {
    clearInterval(this.timerID);
  }

  tick() {
    this.setState({
      date: new Date()
    });
  }

  render() {
    return (
      <div>
        <h1>Hello, world!</h1>
        <FormattedDate date={this.state.date} />
      </div>
    );
  }
}

export default ClockWithUpdate; // Don’t forget to use export default!
