import React from 'react';

const NameUser = (props) => {

  return (
    <h1>Bonjour, {props.name}</h1>
  );
};

export default NameUser;
