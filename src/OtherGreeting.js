import React from 'react';

class OtherGreeting extends React.Component {
  render() {
    return (
      <h1>Bonjour, {this.props.name}</h1>
    );
  }
}

// Spécifie les valeurs par défaut des props :
OtherGreeting.defaultProps = {
  name: 'bel inconnu'
};

export default OtherGreeting;
