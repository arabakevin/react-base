import React from 'react';
import HelloWorld from './HelloWorld';
import NameUser from './NameUser';
import Clock from './Clock';
import Toggle from './Toggle';
import ClockWithUpdate from './ClockWithUpdate';
import NumberList from './NumberList';
import NewForm from './NewForm';
import OtherForm from './OtherForm';
import LoginControl from './LoginControl';
import Calculator from './Calculator';
import SignUpDialog from './SignUpDialog';
import SplitPane from './SplitPane';
import Mouse from './Mouse';
import FollowElement from './FollowElement';
import Table from './Table';
import FilterableProductTable from './FilterableProductTable';
import Greeting from './Greeting';
import StaticGreeting from './StaticGreeting';
import './App.css';

function App() {
  const numbersArray = [1, 2, 3, 4, 5];
  const PRODUCTS = [
    {category: 'Sporting Goods', price: '$49.99', stocked: true, name: 'Football'},
    {category: 'Sporting Goods', price: '$9.99', stocked: true, name: 'Baseball'},
    {category: 'Sporting Goods', price: '$29.99', stocked: false, name: 'Basketball'},
    {category: 'Electronics', price: '$99.99', stocked: true, name: 'iPod Touch'},
    {category: 'Electronics', price: '$399.99', stocked: false, name: 'iPhone 5'},
    {category: 'Electronics', price: '$199.99', stocked: true, name: 'Nexus 7'}
  ];
  return (
    <div className="App">
      <div>
      <HelloWorld />
      <NameUser name="kevin"/>
      <Clock />
      </div>
      <br/><br/><br/><br/>
      <h3> Etat et Cycle de vie </h3>
      <hr/>
      <div>
        <ClockWithUpdate />
      </div>
      <br/><br/><br/><br/>
      <h3> Les conditions </h3>
      <hr/>
      <div>
        <Toggle />
        <LoginControl />
      </div>
      <br/><br/><br/><br/>
      <h3> Liste et clés </h3>
      <hr/>
      <div>
        <NumberList numbers={numbersArray} />
      </div>
      <br/><br/><br/><br/>
      <h3> Formulaire </h3>
      <hr/>
      <div>
        <NewForm />
        <OtherForm />
      </div>
      <br/><br/><br/><br/>
      <h3> Faire remonter l'état </h3>
      <hr/>
      <div>
        <Calculator />
      </div>
      <br/><br/><br/><br/>
      <h3> Composition </h3>
      <hr/>
      <div>
        <SignUpDialog />
      </div>
      <div>
      <SplitPane
        left={
          <HelloWorld />
        }
        right={
          <NameUser name="kevin" />
        } />
      </div>
      <br/><br/><br/><br/>
      <h3> Fragment </h3>
      <hr/>
      <div>
        <Table />
      </div>
      <br/><br/><br/><br/>
      <h3> Props de rendu </h3>
      <hr/>
      <div>
        <h1>Déplacez votre souris sur l’écran !</h1>
        <p>Soyez prudent·e lors de l’utilisation de props de rendu avec React.PureComponent</p>
        <p>React.PureComponent est similaire à React.Component. La seule différence est que React.Component n’implémente pas la méthode shouldComponentUpdate(), alors que React.PureComponent l’implémente en réalisant une comparaison de surface de l’état et des propriétés.
        </p>
        <p>donc ici avec le render on aurait un nouvelle fonction a chaque fois et donc un rafraichissement continuel </p>
        <a href="https://fr.reactjs.org/docs/render-props.html"> Pour plus d'infos.</a>
        <Mouse render={mouse => (
          <FollowElement mouse={mouse} />
        )}/>
      </div>
      <br/><br/><br/><br/>
      <h3> TEST EN REACT </h3>
      <hr/>
      <div>
        <FilterableProductTable products={PRODUCTS} />
      </div>
      <br/><br/><br/><br/>
      <h3> Validation de types avec propTypes  </h3>
      <hr/>
      <a href="https://fr.reactjs.org/docs/typechecking-with-proptypes.html"> Le lien pour plus d'informations.</a>
      <div>
        <Greeting name='kevin' />
        <StaticGreeting name='kevin' />
      </div>
    </div>
  );
}

export default App;
