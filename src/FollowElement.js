import React from 'react';

class FollowElement extends React.Component {
  render() {
    const mouse = this.props.mouse;
    return (
      <div>
        <p> left: {mouse.x} </p>
        <p> top: {mouse.y} </p>
      </div>
    );
  }
}
export default FollowElement;
