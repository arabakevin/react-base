import React from 'react';
import ListItem from './ListItem';

class NumberList extends React.Component  {

  render() {
    const numbersProps = this.props.numbers;
    const listItems = numbersProps.map((number) =>
      // Les clés n’ont besoin d’être uniques qu’au sein de la liste
      <ListItem key={number.toString()} value={number} />
    );
    return (
      <ul>
        {listItems}
      </ul>
    );
  }
}

export default NumberList; // Don’t forget to use export default!
