import React from 'react';

class Mouse extends React.Component {
  constructor(props) {
    super(props);
    this.handleMouseMove = this.handleMouseMove.bind(this);
    this.state = { x: 0, y: 0 };
  }

  handleMouseMove(event) {
    this.setState({
      x: event.clientX,
      y: event.clientY
    });
  }

  render() {
    return (
      <div style={{ height: '100vh' }} onMouseMove={this.handleMouseMove}>

        {/*
          Au lieu de fournir une représentation statique de ce qu’affiche <Mouse>,
          utilisez la prop `render` pour déterminer dynamiquement quoi afficher.
        */}
        {this.props.render(this.state)}
      </div>
    );
  }
}
export default Mouse;
