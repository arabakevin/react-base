import React, { Component } from 'react';

class LoginButton extends Component {
  render() {
    return (
      <button onClick={this.props.onClick}>
        Connexion
      </button>
    );
  }
}

export default LoginButton; // Don’t forget to use export default!
