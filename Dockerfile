FROM node:14 as build-deps
WORKDIR /app
COPY package.json yarn.lock ./
COPY . .
RUN npm install react-scripts -g --silent
RUN yarn install
RUN yarn run build
